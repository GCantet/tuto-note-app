package gc.mobile.app.kotlin.tuto.notepad.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import gc.mobile.app.kotlin.tuto.notepad.R
import gc.mobile.app.kotlin.tuto.notepad.models.Note
import org.w3c.dom.Text

class NoteAdapter (val notes: List<Note>, val itemClickListener: View.OnClickListener) : RecyclerView.Adapter<NoteAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val noteView : CardView = itemView.findViewById<CardView>(R.id.note_card_view)
        val noteTitle : TextView = itemView.findViewById<TextView>(R.id.note_title) as TextView
        val noteContent : TextView = itemView.findViewById<TextView>(R.id.note_content)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_note, parent, false)
        return ViewHolder(viewItem)
    }

    override fun getItemCount(): Int {
        return notes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val note = notes[position]
        holder.noteView.setOnClickListener(itemClickListener)
        holder.noteView.tag = position
        holder.noteTitle.text = note.title
        holder.noteContent.text = note.text
    }


}