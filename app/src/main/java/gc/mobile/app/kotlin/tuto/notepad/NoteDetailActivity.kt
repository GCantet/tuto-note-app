package gc.mobile.app.kotlin.tuto.notepad

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import gc.mobile.app.kotlin.tuto.notepad.dialog.ConfirmDeleteNoteDialogFragment
import gc.mobile.app.kotlin.tuto.notepad.models.Note

class NoteDetailActivity : AppCompatActivity() {

    companion object {
        val REQUEST_EDIT_NOTE = 1
        val EXTRA_NOTE = "note"
        val EXTRA_NOTE_INDEX = "noteIndex"

        val ACTION_SAVE_NOTE = "gc.mobile.app.kotlin.tuto.notepad.actions.ACTION_SAVE_NOTE"
        val ACTION_DELETE_NOTE = "gc.mobile.app.kotlin.tuto.notepad.actions.ACTION_DELETE_NOTE"
    }

    lateinit var note: Note
    var noteIndex: Int = -1

    lateinit var noteDetailTitle: TextView
    lateinit var noteDetailContent: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_detail)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        note = intent.getParcelableExtra<Note>(EXTRA_NOTE)
        noteIndex = intent.getIntExtra(EXTRA_NOTE_INDEX, -1)

        noteDetailTitle = findViewById<TextView>(R.id.note_detail_title)
        noteDetailContent = findViewById<TextView>(R.id.note_detail_content)

        noteDetailTitle.text = note.title
        noteDetailContent.text = note.text

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_note_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {
                saveNote()
                return true
            }
            R.id.action_delete -> {
                showConfirmDeleteNoteDialog()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun showConfirmDeleteNoteDialog() {
        val confirmFragment = ConfirmDeleteNoteDialogFragment(note.title)
        confirmFragment.listener = object: ConfirmDeleteNoteDialogFragment.ConfirmDeleteDialogListener {
            override fun onDialogPositiveClick() {
                deleteNote()
            }

            override fun onDialogNegativeClick() { }
        }
        confirmFragment.show(supportFragmentManager, "confirmDeleteDialog")
    }

    // Gestion de la sauvegarde
    private fun saveNote() {
        note.title = noteDetailTitle.text.toString()
        note.text = noteDetailContent.text.toString()

        intent = Intent(ACTION_SAVE_NOTE)
        intent.putExtra(EXTRA_NOTE, note as Parcelable)
        intent.putExtra(EXTRA_NOTE_INDEX, noteIndex)

        setResult(Activity.RESULT_OK, intent) // Répond au startActivityForResult
        finish()
    }

    private fun deleteNote() {
        intent = Intent(ACTION_DELETE_NOTE)
        intent.putExtra(EXTRA_NOTE_INDEX, noteIndex)

        setResult(Activity.RESULT_OK, intent)
        finish()
    }

}
